@extends('adminlte::page')

@section('css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('content')
    @include('partials.flash-messages')
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <h1>Użytkownicy</h1>
        <div class="col-12 p-2">
            <div class="card">
                <div class="card-header mb-2">
                    <h3 class="card-title">Użytkownicy Api</h3>
                </div>
                <div class="card-body table-responsive p-0 auto-height">
                    <table id="users-table" class="table table-head-fixed text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Dane</th>
                            <th>Nazwa użytkownika</th>
                            <th>Email</th>
                            <th>Telefon</th>
                            <th>Strona</th>
                            <th>Edytuj</th>
                            <th>Usuń</th>
                            <th>Utworzono</th>
                            <th>Edytowano</th>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="id"><label>{{$loop->iteration}}</label></td>
                                <td class="name"><label>{{$user->name}}</label></td>
                                <td class="username"><label>{{$user->username}}</label></td>
                                <td class="email"><label>{{$user->email}}</label></td>
                                <td class="phone"><label>{{$user->phone}}</label></td>
                                <td class="website"><label>{{$user->website}}</label></td>
                                <td class="edit_user">
                                    <a href="{{route('users.edit',$user)}}" class="btn-warning btn">Edytuj</a>
                                </td>
                                <td class="delete_user">
                                    <form action="{{route('users.destroy',$user)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn-danger btn">Usuń</button>
                                    </form>
                                </td>
                                <td class="created_at"><label>{{$user->created_at}}</label></td>
                                <td class="updated_at"><label>{{$user->updated_at}}</label></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
@endsection
