@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h1>Stwórz użytkownika</h1>

                @include('partials.flash-messages')

                <form action="{{ route('users.store')}}" method="POST">
                    @csrf
                    @include('admin.users_forum.partials.partial_create_edit')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Stwórz</button>
                    </div> <!-- form-group// -->
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ asset('js/users/users-create-edit.js') }}"></script>
@stop
