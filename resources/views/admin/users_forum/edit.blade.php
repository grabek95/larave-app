@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h1>Edytuj użytkownika</h1>

                @include('partials.flash-messages')

                <form action="{{ route('users.update', $user) }}" method="POST">
                    @csrf
                    @method('put')
                    @include('admin.users_forum.partials.partial_create_edit')
                    <input name="id" class="form-control" placeholder="" type="number" hidden value="{{ ($user->id)}}">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Edytuj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ asset('js/users/users-create-edit.js') }}"></script>
@stop
