<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
    </div>
    <input name="name" class="form-control" placeholder="Imię i nazwisko" type="text"
           value="{{ $user->name ?? old('name') }}">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
    </div>
    <input name="username" class="form-control" placeholder="Nazwa użytkownika" type="text"
           value="{{ $user->username ?? old('username') }}">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
    </div>
    <input name="email" class="form-control" placeholder="Adres e-mail" type="email"
           value="{{ $user->email ?? old('email') }}">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
    </div>
    <input name="phone" class="form-control" placeholder="Telefon (+48123456789)" type="tel"
           value="{{ ($user->phone ?? old('phone') )}}">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-globe"></i> </span>
    </div>
    <input name="website" class="form-control" placeholder="Strona internetowa" type="url"
           value="{{ ($user->website ?? old('website') )}}">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <label for="address">Adres</label>
</div>

<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> Ulica </span>
    </div>
    <input name="street" class="form-control" placeholder="Ulica" type="text"
           value="@if(isset($user)){{ $user->address()->first()!=null ? $user->address()->first()->street :old('street') }}@endif">
</div> <!-- form-group// -->

<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> Lokal </span>
    </div>
    <input name="suite" class="form-control" placeholder="Lokal" type="text"
           value="@if(isset($user)){{$user->address()->first()!=null ? $user->address()->first()->suite : old('suite') }}@endif">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> Miasto </span>
    </div>
    <input name="city" class="form-control" placeholder="Miasto" type="text"
           value="@if(isset($user)){{ $user->address()->first()!=null ? $user->address()->first()->city : old('city') }}@endif">
</div> <!-- form-group// -->
<div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> Kod pocztowy </span>
    </div>
    <input name="zipcode" class="form-control" placeholder="Kod pocztowy" type="text"
           value="@if(isset($user)){{ $user->address()->first()!=null ? $user->address()->first()->zipcode : old('zipcode') }}@endif">
</div> <!-- form-group// -->
