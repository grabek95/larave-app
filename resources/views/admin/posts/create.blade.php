@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center mb-2">
                @include('partials.flash-messages')

                <form action="{{ route('posts.store') }}" method="POST">
                    @csrf
                    @include('admin.posts.partials.partial_create_edit')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Zapisz</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
@stop
