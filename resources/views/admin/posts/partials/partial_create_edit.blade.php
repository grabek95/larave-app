<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edytuj posts</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Użytkownik</label>
                    @if(isset($post))
                        <input name="username" type="text" class="form-control"
                               placeholder="{{$post->user()->username ?? old('username')}}" disabled="">
                    @else
                        <select name="user_api_id" id="pet-select">
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->username}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Temat</label>
                    <textarea name="title" type="text" class="form-control"
                              placeholder="{{$post->title?? old('title')}}"
                              @if(isset($post))disabled=""@endif></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Zawartość</label>
                    <textarea name="body" class="form-control" rows="3">{{($post->body ?? old('body'))}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
