@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center mb-2">
                @include('partials.flash-messages')

                <form action="{{ route('posts.update', $post) }}" method="POST">
                    @csrf
                    @method('put')
                    @include('admin.posts.partials.partial_create_edit')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Edytuj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
@stop
