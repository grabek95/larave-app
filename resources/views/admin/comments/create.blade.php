@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center mb-2">
                @include('partials.flash-messages')

                <form action="{{ route('comment.store') }}" method="POST">
                    @csrf
                    @include('admin.comments.partials.partial_create_edit')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Zapisz</button>
                    </div>
                </form>
                <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                    <h1>Post</h1>
                    @include('partials.post_partial')
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
@stop
