<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Dodanie/Edytuj komentarz</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Nazwa użytkownika</label>
                    <input name="name" type="text" class="form-control"
                           value="{{$comment->name ?? old('name')}}">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="text" class="form-control"
                           value="{{$comment->email?? old('title')}}"></input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Zawartość</label>
                    <textarea name="body" class="form-control" rows="3">{{($comment->body ?? old('body'))}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<input name="post_id" type="text" class="form-control"
       value="{{$post->id }}" hidden>
