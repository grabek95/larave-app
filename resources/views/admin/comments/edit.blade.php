@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center mb-2">
                @include('partials.flash-messages')

                <form action="{{ route('comment.update', $post) }}" method="POST">
                    @csrf
                    @method('put')
                    @include('admin.comments.partials.partial_create_edit')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> Edytuj</button>
                    </div>
                </form>
                <h1>Post</h1>
                <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                    @include('partials.post_partial')
                </div>
            </div>
        </div>

    </div>
@stop
@section('js')
@stop
