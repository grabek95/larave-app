<div class="col-12 p-2">
    <div class="card card-warning">
        <div class="card-header ">
            <h3 class="card-title"><b>Temat</b> : {{$post->title}}</h3>
        </div>
        <div class="card-header mb-2">
            <h7><b>Użytkownik</b> : {{$post->user()->username}}</h7>
        </div>
        <div class="card-body table-responsive p-0 auto-height ">
            <div class="container">
                {{$post->body}}
            </div>
        </div>
    </div>
</div>

