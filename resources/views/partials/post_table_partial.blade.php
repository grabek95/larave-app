<div class="col-12 p-2">
    <div class="card">
        <div class="card-header mb-2">
            <h3 class="card-title">Posty</h3>
        </div>
        <div class="card-body table-responsive p-0 auto-height">
            <table id="posts-table" class="table table-head-fixed text-nowrap">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Post</th>
                    <th>Autor</th>
                    @auth
                        <th>
                            Edytuj
                        </th>
                        <th>
                            Usuń
                        </th>
                    @endauth
                    <th>Utworzono</th>
                    <th>Edytowano</th>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td class="id"><label>{{$post->id}}</label></td>
                        <td class="post_id"><label><a href="
                                @Auth
                                {{route('posts.show',$post->id)}}
                                @else
                                {{route('user_post.show',$post->id)}}
                                @endif
                                    ">{{$post->title}}</a></label></td>
                        <td class="user"><label>
                                {{$post->user()->username}}
                            </label>
                        </td>
                        @auth
                            <td class="edit_user">
                                <a href="{{route('posts.edit',$post)}}" class="btn-warning btn">Edytuj</a>
                            </td>
                            <td class="delete_user">
                                <form action="{{route('posts.destroy',$post)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-danger btn">Usuń</button>
                                </form>
                            </td>
                        @endauth
                        <td class="created_at"><label>{{$post->created_at}}</label></td>
                        <td class="updated_at"><label>{{$post->updated_at}}</label></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $posts->links() }}
        </div>
    </div>
</div>

