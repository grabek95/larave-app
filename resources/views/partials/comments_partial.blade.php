<div class="col-12 p-2">
    <div class="card">
        <div class="card-header mb-2">
            <h3 class="card-title">Komentarze</h3>
        </div>
        <div class="card-body table-responsive p-0 auto-height">
            @auth
                <form action="{{route('comment.create')}}">
                    <input name="post_id" type="text" hidden value="{{$post->id}}">
                    <button type="submit" class="btn-primary btn btn">Dodaj</button>
                </form>
            @endauth
            <table id="posts-table" class="table table-head-fixed text-nowrap">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Użytkownik</th>
                    <th>Email</th>
                    <th>Treść</th>
                    @auth
                        <th>
                            Edytuj
                        </th>
                        <th>
                            Usuń
                        </th>
                    @endauth
                    <th>Utworzono</th>
                    <th>Edytowano</th>
                </thead>
                <tbody>
                @foreach($post->comments()->get() as $comment)
                    <tr>
                        <td class="id"><label>{{$loop->iteration}}</label></td>
                        <td class="user"><label>{{$comment->name}}</label></td>
                        <td class="email"><label>{{$comment->email}}</label></td>
                        <td class="body"><label>{!! nl2br($comment->body) !!}</label></td>
                        @auth
                            <td class="edit_user">
                                <a href="{{route('comment.edit',$comment)}}" class="btn-warning btn">Edytuj</a>
                            </td>
                            <td class="delete_user">
                                <form action="{{route('comment.destroy',$comment)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-danger btn">Usuń</button>
                                </form>
                            </td>
                        @endauth
                        <td class="created_at"><label>{{$comment->created_at}}</label></td>
                        <td class="updated_at"><label>{{$comment->updated_at}}</label></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
