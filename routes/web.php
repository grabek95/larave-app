<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PostsApiController::class, 'postsWithoutAuth']);
Route::get('/post/{id}', [\App\Http\Controllers\PostsApiController::class, 'getByIdWithoutAuth'])->name('user_post.show');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::resource('posts', App\Http\Controllers\PostsApiController::class);
    Route::resource('users', App\Http\Controllers\UserApiController::class)->except('show');
    Route::resource('comment', App\Http\Controllers\CommentsController::class)->except('index', 'show');

});
