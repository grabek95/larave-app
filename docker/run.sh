docker rm $(docker ps -a -q) -f #removes your containers
#docker volume rm $(docker volume ls -q)
docker network prune -f #remove all unused networks
#docker-compose build --no-cache #uncomment if you want to reinstall containers
docker-compose up -d #running your containers
docker exec -it my-app composer install
docker exec -it my-app php artisan config:cache
docker exec -it my-app php artisan config:clear
docker exec -it my-app php artisan cache:clear
docker exec -it my-app php artisan view:clear
docker exec -it my-app php artisan migrate
docker exec -it my-app rm bootstrap/cache/*.php -f
docker exec -it my-app rm storage/logs/*.log -f
