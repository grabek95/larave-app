<?php

namespace App\Providers;


use App\Services\ApiServices\CommentsService\CommentsApiService;
use App\Services\ApiServices\CommentsService\CommentsApiServiceInterface;
use App\Services\ApiServices\PostsService\PostsApiService;
use App\Services\ApiServices\PostsService\PostsApiServiceInterface;
use App\Services\ApiServices\UserService\UsersApiService;
use App\Services\ApiServices\UserService\UsersApiServiceInterface;
use Illuminate\Support\ServiceProvider;

class ApiServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UsersApiServiceInterface::class, UsersApiService::class);
        $this->app->bind(PostsApiServiceInterface::class, PostsApiService::class);
        $this->app->bind(CommentsApiServiceInterface::class, CommentsApiService::class);

    }

}
