<?php

namespace App\Http\Controllers;

use App\Models\AddressApi;
use App\Models\UserApi;
use Exception;
use Illuminate\Http\Request;

class UserApiController extends Controller
{
    public function index()
    {
        return view('admin.users_forum.index', [
            'users' => UserApi::all()
        ]);
    }

    public function create()
    {
        return view('admin.users_forum.create');
    }

    public function edit(Request $request, $id)
    {
        return view('admin.users_forum.edit', [
            'user' => UserApi::find($id)
        ]);
    }

    public function store(Request $request)
    {
        $user = $request->only(['name', 'username', 'email', 'phone', 'website']);
        $userDb = UserApi::create($user);
        $address = $request->only(['street', 'suite', 'city', 'zipcode']);
        AddressApi::create(array_merge($address, ['user_api_id' => $userDb->id]));
        return redirect()->route('users.index')->with('success', 'Utworzono użytkownika!');
    }

    public function update(Request $request, $user)
    {
        $userInputs = $request->only(['name', 'username', 'email', 'phone', 'website']);
        $userDB = UserApi::find($user);
        $userDB->name = $userInputs['name'];
        $userDB->username = $userInputs['username'];
        $userDB->email = $userInputs['email'];
        $userDB->phone = $userInputs['phone'];
        $userDB->website = $userInputs['website'];
        $userDB->save();
        $address = $request->only(['street', 'suite', 'city', 'zipcode']);
        $addressDB = AddressApi::where('user_api_id' , $userDB->id)->first();
        $addressDB = $addressDB ?: new AddressApi();
        $addressDB->user_api_id = $userDB->id;
        $addressDB->street = $address['street'];
        $addressDB->suite = $address['suite'];
        $addressDB->city = $address['city'];
        $addressDB->zipcode = $address['zipcode'];
        $addressDB->save();
        return redirect()->route('users.index')->with('success', 'Zaktualizowano użytkownika!');
    }

    public function destroy(Request $request, $id)
    {
        try {
            UserApi::find($id)->delete();
            $result = ['status' => 'success', 'message' => 'Usunięto użytkownika!'];
        } catch (Exception $e) {
            $result = ['status' => 'error', 'message' => 'Wystąpił błąd!'];
        }
        return redirect()->route('users.index')->with($result['status'], $result['message']);
    }
}
