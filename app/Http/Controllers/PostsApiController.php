<?php

namespace App\Http\Controllers;

use App\Models\PostApi;
use App\Models\UserApi;
use Illuminate\Http\Request;
use Exception;

class PostsApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'getById', 'edit', 'upload', 'create', 'store', 'destroy']]);
    }

    public function postsWithoutAuth()
    {
        return view('posts.index', [
            'posts' => PostApi::paginate(10)
        ]);
    }

    public function getByIdWithoutAuth(Request $request, $id)
    {
        return view('posts.show', [
            'post' => PostApi::find($id)
        ]);
    }

    public function index()
    {
        return view('admin.posts.index', [
            'posts' => PostApi::paginate(10)
        ]);
    }

    public function show(Request $request, $post)
    {
        return view('admin.posts.show', [
            'post' => PostApi::find($post)
        ]);
    }

    public function edit(Request $request, $post)
    {
        return view('admin.posts.edit', [
            'post' => PostApi::find($post)
        ]);
    }

    public function update(Request $request, $post)
    {
        $postInputs = $request->only('body');
        $postDB = PostApi::find($post);
        if ($postDB) {
            $postDB->body = $postInputs['body'];
            $postDB->save();
            return redirect()->route('posts.index')->with('success', 'Zaktualizowano post!');
        }
        return redirect()->route('posts.index')->with('error', 'Wystąpił błąd');

    }

    public function create(Request $request)
    {
        return view('admin.posts.create', [
            'users' => UserApi::all()
        ]);
    }

    public function store(Request $request)
    {
        $postInputs = $request->only('user_api_id', 'title', 'body');
        $postDB = PostApi::create($postInputs);
        if ($postDB) {
            $postDB->body = $postInputs['body'];
            $postDB->save();
            return redirect()->route('posts.index')->with('success', 'Dodano post!');
        }
        return redirect()->route('posts.index')->with('error', 'Wystąpił błąd');

    }

    public function destroy(Request $request, $id)
    {
        try {
            PostApi::find($id)->delete();
            $result = ['status' => 'success', 'message' => 'Usunięto post!'];
        } catch (Exception $e) {
            $result = ['status' => 'error', 'message' => 'Wystąpił błąd!'];

        }
        return redirect()->route('posts.index')->with($result['status'], $result['message']);
    }

}
