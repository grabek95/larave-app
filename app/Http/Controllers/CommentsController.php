<?php

namespace App\Http\Controllers;

use App\Models\CommentApi;
use App\Models\CompanyApi;
use App\Models\PostApi;
use App\Models\UserApi;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function edit(Request $request, $comment)
    {
        $comment = CommentApi::find($comment);
        return view('admin.comments.edit', [
            'comment' => $comment,
            'post' => $comment->post()
        ]);
    }

    public function update(Request $request, $comment)
    {
        $commentInputs = $request->only('body', 'name', 'email');
        $commentDB = CommentApi::find($comment);
        if ($commentDB) {
            $commentDB->name = $commentInputs['name'];
            $commentDB->body = $commentInputs['body'];
            $commentDB->email = $commentInputs['email'];
            $commentDB->save();
            return redirect()->route('posts.show', $commentDB->post_id)->with('success', 'Zaktualizowano komentarz!');
        }
        return redirect()->route('posts.index')->with('error', 'Wystąpił błąd');

    }

    public function create(Request $request)
    {
        return view('admin.comments.create', [
            'post' => PostApi::find($request->get('post_id'))
        ]);
    }

    public function store(Request $request)
    {
        $commentInputs = $request->only('post_id', 'name', 'email', 'body');
        $commentDB = CommentApi::create($commentInputs);
        if ($commentDB) {
            $commentDB->body = $commentInputs['body'];
            $commentDB->save();
            return redirect()->route('posts.show', PostApi::find($commentInputs['post_id']))->with('success', 'Dodano komentarz!');
        }
        return redirect()->route('posts.index')->with('error', 'Wystąpił błąd');

    }

    public function destroy(Request $request, $id)
    {
        $comment = CommentApi::find($id);
        $post = $comment->post();
        try {
            $comment->delete();
            $result = ['status' => 'success', 'message' => 'Usunięto komentarz!'];
        } catch (Exception $e) {
            $result = ['status' => 'error', 'message' => 'Wystąpił błąd!'];
        }
        return redirect()->route('posts.show', $post)->with($result['status'], $result['message']);
    }
}
