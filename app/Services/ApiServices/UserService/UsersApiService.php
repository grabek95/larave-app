<?php

namespace App\Services\ApiServices\UserService;

use App\Models\AddressApi;
use App\Models\UserApi;
use App\Services\ApiServices\ApiService\ApiService;

class UsersApiService extends ApiService implements UsersApiServiceInterface
{

    public function getAndSaveUsers()
    {
        $users = $this->getRequestToCollect(self::URL, self::METHOD['getUsers']);
        $this->saveUsers($users);
    }

    public function saveUsers($users)
    {
        $usersToSave = [];
        $addressesToSave = [];
        foreach ($users as $user) {
            $usersToSave[] = collect($user)->except(['address', 'company'])->toArray();
            $addressesToSave[] = array_merge(collect($user['address'])->except(['geo'])->toArray(), [
                'lat' => $user['address']['geo']['lat'],
                'lng' => $user['address']['geo']['lng'],
                'user_api_id' => $user['id']
                ]);
        }
        UserApi::upsert($usersToSave,['id','email'],['id','name','username','email','phone','website']);
        AddressApi::upsert($addressesToSave,'user_api_id');

    }
}
