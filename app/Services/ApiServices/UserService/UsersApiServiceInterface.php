<?php

namespace App\Services\ApiServices\UserService;

interface UsersApiServiceInterface
{
    public function getAndSaveUsers();

    public function saveUsers($users);
}
