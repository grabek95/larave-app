<?php

namespace App\Services\ApiServices\PostsService;

use App\Models\PostApi;
use App\Services\ApiServices\ApiService\ApiService;

class PostsApiService extends ApiService implements PostsApiServiceInterface
{

    public function getAndSavePosts()
    {
        $posts = $this->getRequestToCollect(self::URL, self::METHOD['getPosts']);
        $this->savePosts($posts);
    }


    public function savePosts($posts)
    {
        $postsToSave = array_map(fn($value): array => ['user_api_id' => $value['userId'], 'id' => $value['id'], 'title' => $value['title'], 'body' => $value['body']], $posts->toArray());
        PostApi::upsert($postsToSave, ['id', 'user_api_id'], ['title', 'body']);
    }
}
