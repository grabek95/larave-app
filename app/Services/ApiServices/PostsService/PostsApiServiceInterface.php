<?php

namespace App\Services\ApiServices\PostsService;

interface PostsApiServiceInterface
{
    public function getAndSavePosts();

    public function savePosts($posts);
}
