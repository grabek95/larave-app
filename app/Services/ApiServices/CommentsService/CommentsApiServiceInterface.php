<?php

namespace App\Services\ApiServices\CommentsService;

interface CommentsApiServiceInterface
{
    public function getAndSaveComments();

    public function saveComments($comments);
}
