<?php

namespace App\Services\ApiServices\CommentsService;

use App\Models\CommentApi;
use App\Models\PostApi;
use App\Services\ApiServices\ApiService\ApiService;

class CommentsApiService extends ApiService implements CommentsApiServiceInterface
{

    public function getAndSaveComments()
    {
        $comments = $this->getRequestToCollect(self::URL, self::METHOD['getComments']);
        $this->saveComments($comments);
    }


    public function saveComments($comments)
    {
        $commentsToSave = array_map(fn($value): array => ['post_id' => $value['postId'], 'id' => $value['id'], 'name' => $value['name'], 'email' => $value['email'], 'body' => $value['body']], $comments->toArray());
        CommentApi::upsert($commentsToSave, ['id', 'post_id'], ['body', 'updated_at']);
    }
}
