<?php

namespace App\Services\ApiServices\ApiService;

use http\Exception\RuntimeException;
use Illuminate\Support\Facades\Http;

abstract class ApiService
{
    protected const URL = "https://jsonplaceholder.typicode.com";
    protected const METHOD = [
        'getUsers' => '/users',
        'getPosts' => '/posts',
        'getComments' => '/comments',
    ];
    public function getRequestToCollect($url, $method)
    {
        try {
            $result = Http::get($url.$method)->collect();
        }catch (\Exception $e){
            throw new RuntimeException($e->getMessage());
        }
        return $result;
    }
}
