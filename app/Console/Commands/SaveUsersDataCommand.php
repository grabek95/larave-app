<?php

namespace App\Console\Commands;

use App\Jobs\GenerateDataCommentsToDatabaseJob;
use App\Jobs\GenerateDataPostsToDatabaseJob;
use App\Jobs\GenerateDataUsersToDatabaseJob;
use Illuminate\Console\Command;

class SaveUsersDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saveUsersDataCommand:weekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save posts_api every hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            GenerateDataUsersToDatabaseJob::dispatch();
            GenerateDataPostsToDatabaseJob::dispatch();
            GenerateDataCommentsToDatabaseJob::dispatch();
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
