<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostApi extends Model
{
    use HasFactory;

    protected $table = 'posts_api';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(UserApi::class, 'user_api_id', 'id')->first();
    }

    public function comments()
    {
        return $this->hasMany(CommentApi::class, 'post_id', 'id');
    }
}
