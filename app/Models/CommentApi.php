<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentApi extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'comments_api';

    public function post()
    {
        return $this->belongsTo(PostApi::class)->first();
    }
}
