<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserApi extends Model
{
    use HasFactory;

    protected $table = 'users_api';
    protected $guarded = [];

    public function address()
    {
        return $this->hasOne(AddressApi::class);
    }

    public function posts()
    {
        return $this->hasMany(PostApi::class);
    }
}
